#                          				  **JEU DE REVERSI**

Jeu de reversi / othello fait en C++ 	

![Capture d'écran IMGUR sur le jeu reversi.](C:\Users\dbois\OneDrive\Documents\qt_reversi\reversi\reversi.png)

## 													Fonctionnement de l'application

> Pour faire fonctionner le jeu, il suffit de, chacun son tour, placer un pion de sa couleur.
>
> Pour placer ce pion, il faut entré la ligne (de 1 à 8) et la colonne (de A à H) et appuyer sur le bouton <Jouer>
>
> Le pion doit absolument être placé à coté d'un pion déjà placé et doit absolument capturer un des pions adverses, sinon le pion ne sera pas placé.
>
> Pour capture un pion adverse, il suffit de coincer un ou plusieurs pions de la couleur adverse entre deux pion de notre couleur.
>
> La partie se termine lorsque le tableau est rempli, le gagnant est alors décidé par le nombre de pion que chaque joueur a, le plus grand nombre gagne.
>
> Si les deux joueurs passent leurs tours, il est possible d'utiliser le bouton <Terminer la partie> pour que la partie se finisse instantanément.



### 																			Fait par Dale Boisjoli



