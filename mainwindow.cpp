#include "mainwindow.h"

#include <QFormLayout>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QMessageBox>


/*!
 * \brief MainWindow::FenetrePrincipale
 * Construction du tableau de jeu.
 * \param parent le widget dans lequel insérer cette fenêtre.
 *
 * Le tableau consiste en une grille de jeu, d'un champ de texte
 * pour faire l'entrée de la coordonnée où insérer les pions blanc et noir, et
 * d'un bouton pour jouer le coup.
 */
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QWidget *fenetre = new QWidget(this);
    QVBoxLayout *grille = new QVBoxLayout();
    fenetre->setLayout(grille);

    // 1
    scene = new QGraphicsScene;
    initialiserJeu();
    QGraphicsView *view = new QGraphicsView(scene);
    grille->addWidget(view);

    choixXY = new QLineEdit;
    choixXY->setFocus();
    choixXY->setInputMask("A9");
    QFormLayout *xyChoisieForm = new QFormLayout;
    xyChoisieForm->addRow("XY", choixXY);
    grille->addLayout(xyChoisieForm);
    choixXY->setCursorPosition(0);

    QPushButton *boutonJouer = new QPushButton("Jouer");
    grille->addWidget(boutonJouer);
    // 3
    connect(boutonJouer, &QPushButton::clicked, this, &MainWindow::slotJouer);

    QPushButton *boutonPasserTour = new QPushButton("Passer son tour");
    grille->addWidget(boutonPasserTour);
    connect(boutonPasserTour, &QPushButton::clicked, this, &MainWindow::slotPasserTour);

    QPushButton *boutonTerminerPartie = new QPushButton("Terminer la partie");
    connect(boutonTerminerPartie, &QPushButton::clicked, this, &MainWindow::slotTerminerPartie);
    grille->addWidget(boutonTerminerPartie);

    setCentralWidget(fenetre);
}

MainWindow::~MainWindow()
{
}

/*!
 * \brief MainWindow::initialiserJeu
 *          mise à 0 de toutes les cellules du jeu
 *          sauf les 4 du milieu
 */
void MainWindow::initialiserJeu()
{
    // 7
    for(int i=0; i<NBR_CELL; i++) {
        for(int j=0; j<NBR_CELL; j++) {
            tableau[i][j] = 0;
        }
    }

    // Initialisation du plateau de départ de Reversi
    tableau[3][3] = 2;
    tableau[4][4] = 2;
    tableau[3][4] = 1;
    tableau[4][3] = 1;

    dessinerJeu();
}

/*!
 * \brief MainWindow::dessinerJeu
 *  Affiche le système de coordonnées autour des cases
 *  ainsi que les cases du jeu.
 *
 *  Le tableau a NBR_CELL x NBR_CELL, et la taille des
 *  carrés est définie par TAILLE
 *
 *  Les coordonnées des colonnes sont déterminées par ETIQUETTE.
 *  Les coordonnées des lignes sont numériques et partent à 1.
 */
void MainWindow::dessinerJeu() {
    scene->clear();

    for(int i =0; i<NBR_CELL; i++) {
        // 2
        // dessine les coordonnées
        QGraphicsTextItem *txt = new QGraphicsTextItem();
        txt->setPlainText(ETIQUETTE[i]);
        txt->setPos((i+1)*(TAILLE)+TAILLE/2, 20);
        scene->addItem(txt);
        txt = new QGraphicsTextItem();
        txt->setPlainText(QString::number(i+1));
        txt->setPos(20, (i+1.5)*(TAILLE));
        scene->addItem(txt);

        //dessine les cases
        for(int j=0; j<NBR_CELL; j++) {
            QGraphicsRectItem *rectItem =
                    new QGraphicsRectItem(QRectF((i+1)*TAILLE, (j+1)*TAILLE, TAILLE, TAILLE));
            if((i+j)%2 == 1) {
                rectItem->setBrush(Qt::gray);
            } else {
                rectItem->setBrush(Qt::white);
            }
            scene->addItem(rectItem);
            // 6
            //dessine les pieces
            if(tableau[i][j] != 0)
            {
                // 8
                dessinerPiece(i,j,tableau[i][j]);
            }
        }
    }
}


/*!
 * \brief MainWindow::dessinerPiece
 * Affichage des pions blanc ou noir
 * \param aCol[in], aLigne[in] les coordonnées où insérer la pièce
 * \param aJoueurId[in] L'id du joueur: 1=noir, 2=blanc (0 est réservé pour les cases vides)
 */
void MainWindow::dessinerPiece(int aCol, int aLigne, int aJoueurId) {

    QGraphicsPixmapItem *item;
    if(aJoueurId==1) {
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/noir.png"));
    } else {
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/blanc.png"));
    }
    item->setPos((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE);
    item->setScale(0.25);
    scene->addItem(item);
};


/*!
 * \brief MainWindow::placerPion
 * Place le pion du joueur lorsqu'il donne la colonne et la ligne ou il veut la placer
 * \param int colonne la colonne ou le pion sera mit
 * \param int ligne la ligne ou le pion sera mit
 */
bool MainWindow::placerPion(int colonne, int ligne)
{

    if (tableau[colonne][ligne] != 0) {
        return false;
    }

    bool capture = false;
    int joueurCourant = joueur;
    int adversaire = (joueurCourant == 1) ? 2 : 1;

    // Vérifie les cases adjacente pour la capture
    for (int dirX = -1; dirX <= 1; dirX++) {
        for (int dirY = -1; dirY <= 1; dirY++) {
            if (dirX == 0 && dirY == 0) {
                continue; // Ne pas vérifier la case courante
            }

            int x = colonne + dirX;
            int y = ligne + dirY;
            int nbPionsAdverses = 0;
            while (x >= 0 && x < NBR_CELL && y >= 0 && y < NBR_CELL && tableau[x][y] == adversaire) {
                x += dirX;
                y += dirY;
                nbPionsAdverses++;
            }
            if (x >= 0 && x < NBR_CELL && y >= 0 && y < NBR_CELL && nbPionsAdverses > 0 && tableau[x][y] == joueurCourant) {
                // On peut capturer les pions adverses dans cette direction
                capture = true;
                x = colonne + dirX;
                y = ligne + dirY;
                while (x >= 0 && x < NBR_CELL && y >= 0 && y < NBR_CELL && tableau[x][y] == adversaire) {
                    tableau[x][y] = joueurCourant;
                    x += dirX;
                    y += dirY;
                }
            }
        }
    }
    //Vérifie si le tableau de jeu est plein
    if (capture) {
        tableau[colonne][ligne] = joueurCourant;
        bool tableauPlein = true;
        for (int i = 0; i < NBR_CELL; i++) {
            for (int j = 0; j < NBR_CELL; j++) {
                if (tableau[i][j] == 0) {
                    tableauPlein = false;
                    break;
                }
            }
            if (!tableauPlein) {
                break;
            }
        }
    //Si le tableau est plein, termine la partie
        if (tableauPlein) {
            calculerPoints();
            scene->clear();
            joueur = 1;
            initialiserJeu();
        } else if (capture) {
            return true;
        } else {
            joueur = (joueur == 1) ? 2 : 1;
            return false;
        }
}
}

/**
 * @brief MainWindow::calculerPoints
 * Calcul le nombre de pion de chaque couleur présent à la fin de la partie et détermine le gagnant
 */
void MainWindow::calculerPoints() {
    nbPionsNoirs = 0;
    nbPionsBlancs = 0;

    for (int i = 0; i < NBR_CELL; i++) {
        for (int j = 0; j < NBR_CELL; j++) {
            if (tableau[i][j] == 1) {
                nbPionsNoirs++;
            } else if (tableau[i][j] == 2) {
                nbPionsBlancs++;
            }
        }
    }
    if (nbPionsNoirs > nbPionsBlancs) {
            QMessageBox::information(nullptr, "Fin de partie", "Le joueur Noir a gagné avec " + QString::number(nbPionsNoirs) + " points !");
        } else if (nbPionsBlancs > nbPionsNoirs) {
            QMessageBox::information(nullptr, "Fin de partie", "Le joueur Blanc a gagné avec " + QString::number(nbPionsBlancs) + " points !");
        } else {
            QMessageBox::information(nullptr, "Fin de partie", "La partie est terminée. Les deux joueurs ont un score de " + QString::number(nbPionsNoirs) + " points !");
        }
    }

    // Slots
/*!
 * \brief MainWindow::slotJouer
 *   Place un pion blanc ou un pion noir sur le jeu.
 *
 *   La colonne est dans choixXY[0], et la ligne dans choixXY[1]
 *
 *  La colonne doit être dans ETIQUETTE.
 *
 *  La ligne doit être un numérique entre 1 et NBR_CELL inclusivement.
 */
void MainWindow::slotJouer() {
    if(!(choixXY->text().isEmpty())) {
        QString colTxt = choixXY->text()[0];
        QString ligneTxt = choixXY->text()[1];
        // conversion en index numérique de la lettre représentant la colonne
        int col = ETIQUETTE.indexOf(colTxt.toUpper());
        int ligne = ligneTxt.toInt()-1;

        // vérification de la validité des coordonnées
        if (col >= 0 && col < NBR_CELL && ligne >= 0 && ligne < NBR_CELL) {
            if (placerPion(col, ligne)) {
                dessinerJeu();
                //Calcul de l'ID du prochain joueur
                joueur =  ((joueur)%2)+1;
            }
        }
    }
    choixXY->clear();
    choixXY->setFocus();
    choixXY->setCursorPosition(0);
}


/*!
 * \brief MainWindow::slotPasserTour
 *   Passe le tour du joueur en cours
 */
void MainWindow::slotPasserTour() {
    joueur = ((joueur) % 2) + 1;
    nbToursJoueurSansAction++;
}

/*!
 * \brief MainWindow::slotTerminerPartie
 *   Calcule les points de chaque joueur et affiche le gagnant
 *   ré-initialise le tableau de jeu et met en cours le tour du joueur 1
 */
void MainWindow::slotTerminerPartie() {
    if (nbToursJoueurSansAction > 1) {
       calculerPoints();
       scene->clear();
       joueur = 1;
       initialiserJeu();
    }
}



