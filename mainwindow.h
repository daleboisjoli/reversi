#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QMainWindow>

class QSoundEffect;
class QGraphicsScene;
class QLineEdit;
#define TAILLE 50
#define NBR_CELL 8

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void slotJouer();
    void slotPasserTour();
    void slotTerminerPartie();

private:
    int joueur = 1; /**< id du joueur en cours  */
    int tableau[NBR_CELL][NBR_CELL]; /**< Les valeurs du tableau de jeu */

    const QString ETIQUETTE = "ABCDEFGHI"; /**< étiquette des coordonnées */
    QLineEdit *choixXY; /**< le champ pour entrer les coordonnées où jouer */
    QGraphicsScene *scene; /**< la scène où est dessiné le jeu */
    int nbToursJoueurSansAction; /**< Nombre de tour passés  */
    int nbPionsNoirs, nbPionsBlancs; /**< Nombre de pion blanc et noir  */

    void dessinerJeu();
    void initialiserJeu();
    void rafraichirJeu(int aCol, int aLigne);
    void dessinerPiece(int aCol, int aLigne, int aJoueurId);
    bool placerPion(int colonne, int ligne);
    void calculerPoints();
    bool finDePartie();

};
#endif // FENETREPRINCIPALE_H

